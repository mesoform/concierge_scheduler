#!/usr/bin/env bash

# Script to manage application containers at scale

# Environment setup
cd "$(dirname "$0")"
shopt -s expand_aliases
. ./docker_env.sh

# Variable assignment
action=$1
service_name=$2
current_scale=$3
increment=$4

TOKEN=$(curl -s -X POST -H 'Content-Type: application/json-rpc' -d "{    \"jsonrpc\": \"2.0\",    \"method\": \"user.login\",    \"params\": {        \"user\": \"$ZBX_USER\",        \"password\": \"$ZBX_PASS\"    },    \"id\": 1,    \"auth\": null }" http://$ZABBIX_URL/api_jsonrpc.php| jq -r .result)

SERVICE=undefined

curl -s -X POST -H 'Content-Type: application/json-rpc' -d "{\"jsonrpc\": \"2.0\", \"method\": \"host.get\", \"params\": {\"output\": [\"host\"], \"selectInventory\": [ \"notes\" ], \"searchInventory\": { \"name\": \"$service_name\" }},\"auth\": \"$TOKEN\",\"id\": 2}" http://$ZABBIX_URL/api_jsonrpc.php | jq -r .result[].inventory.notes > ${DOCKER_CERT_PATH}/key.pem
curl -s -X POST -H 'Content-Type: application/json-rpc' -d "{\"jsonrpc\": \"2.0\", \"method\": \"host.get\", \"params\": {\"output\": [\"host\"], \"selectInventory\": [ \"poc_1_notes\" ], \"searchInventory\": { \"name\": \"$service_name\" }},\"auth\": \"$TOKEN\",\"id\": 2}" http://$ZABBIX_URL/api_jsonrpc.php | jq -r .result[].inventory.poc_1_notes > ${DOCKER_CERT_PATH}/ca.pem
curl -s -X POST -H 'Content-Type: application/json-rpc' -d "{\"jsonrpc\": \"2.0\", \"method\": \"host.get\", \"params\": {\"output\": [\"host\"], \"selectInventory\": [ \"poc_2_notes\" ], \"searchInventory\": { \"name\": \"$service_name\" }},\"auth\": \"$TOKEN\",\"id\": 2}" http://$ZABBIX_URL/api_jsonrpc.php | jq -r .result[].inventory.poc_2_notes > ${DOCKER_CERT_PATH}/cert.pem

scale_service(){
    /usr/local/bin/docker-compose --tlsverify --tlscert=${DOCKER_CERT_PATH}/cert.pem \
       --tlscacert=${DOCKER_CERT_PATH}/ca.pem --tlskey=${DOCKER_CERT_PATH}/key.pem \
       --host=${DOCKER_HOST} --file /tmp/docker-compose.yml --project-name dockerlx \
       scale ${service_name}=$1
    echo "$(date): Scaled ${service_name} from ${current_scale} to $1" >> /tmp/app_scheduler_output
    exit 0
 }

scale_up(){
    desired_scale=$((current_scale + increment))
    scale_service ${desired_scale}
}

scale_down(){
    desired_scale=$((current_scale - increment))
    scale_service ${desired_scale}
}

increase_cpu(){
    # resize container with more CPU resources
    echo "increase_cpu"
}

decrease_cpu(){
    # resize container with less resources
    echo "decreate_cpu"
}

service_ps(){
    docker-compose -f /tmp/docker-compose.yml -p dockerlx ps
}

# run specified action
${action}