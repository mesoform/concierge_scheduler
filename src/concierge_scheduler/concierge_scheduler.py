import os
import sys
import logging
import json
from pyzabbix import ZabbixAPI, ZabbixAPIException
from collections import defaultdict
import subprocess

__ZBX_API_DEV = 'secondary.gorillass.co.uk:10052'
#__ZBX_API_DEV = 'zabbix-web.inst.gaz.gb-home.cns.gorillass.co.uk'
#__ZBX_API_DEV = 'zbxweb.svc.eb49da3d-7240-6e94-8e93-b65f3954c652.us-east-1.triton.zone'
__ENV_ZABBIX_API_HOST = 'ZBX_API_HOST'

action = sys.argv[1] if len(sys.argv) > 1 else 0
service_name = sys.argv[2] if len(sys.argv) > 2 else 0
current_scale = int(sys.argv[3]) if len(sys.argv) > 3 else 0
increment = int(sys.argv[4]) if len(sys.argv) > 4 else 0


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    stream = logging.StreamHandler()
    fmt = logging.Formatter('%(asctime)s [%(threadName)s] '
                            '[%(name)s] %(levelname)s: %(message)s')
    stream.setFormatter(fmt)
    logger.addHandler(stream)

    return logger


__LOG = get_logger(__name__)


def __info(message, *args):
    __LOG.log(logging.INFO, message.format(*args))


def __log_error_and_fail(message, *args):
    __LOG.log(logging.ERROR, message.format(*args))
    sys.exit(-1)


__zbx_api = None


def initiate_zabbix_api(zbx_host, zbx_user, zbx_password):
    global __zbx_api
    zbx_url = 'http://{}'.format(zbx_host)
    __info('Logging in using url={} ...', zbx_url)
    __zbx_api = ZabbixAPI(zbx_url)
    __zbx_api.login(user=zbx_user, password=zbx_password)
    __info('Connected to Zabbix API Version {} as {}', __zbx_api.api_version(), zbx_user)


def create_pem_file(inv_property, filename, service_name):
    pem_file = __zbx_api.host.get(output=["host"], selectInventory=[inv_property], searchInventory={"alias": service_name})
    __info('Generating file {}.pem ...', filename)
    file = open('{}/{}.pem'.format(DOCKER_CERT_PATH, filename), 'w')
    file.write(pem_file[0]["inventory"][inv_property])


def scale_service(desired_scale):
   subprocess.run("/usr/local/bin/docker-compose --tlsverify --tlscert={}/cert.pem \
                  --tlscacert={}/ca.pem --tlskey={}/key.pem \
                  --host={} --file /tmp/docker-compose.yml --project-name dockerlx \
                  scale {}={}".format(DOCKER_CERT_PATH, DOCKER_CERT_PATH, DOCKER_CERT_PATH, DOCKER_HOST, service_name, desired_scale).split())
   __info("Scaled {} from {} to {}".format(service_name, current_scale, desired_scale))


def scale_up(current_scale, increment):
    desired_scale=(current_scale + increment)
    scale_service(desired_scale)


def scale_down(current_scale, increment):
    desired_scale=(current_scale - increment)
    scale_service(desired_scale)


def service_ps(*args):
    subprocess.run("docker-compose -f /tmp/docker-compose.yml -p dockerlx ps".split())


if __name__ == '__main__':
    from docker_env import *

    host = os.getenv(__ENV_ZABBIX_API_HOST) or ZABBIX_URL
    user = ZBX_USER
    password = ZBX_PASS

    initiate_zabbix_api(host, user, password)
    create_pem_file("notes", 'key', service_name)
    create_pem_file("poc_1_notes", 'ca', service_name)
    create_pem_file("poc_2_notes", 'cert', service_name)
    globals()[action](current_scale, increment)
