export ZBX_USER=Admin
export ZBX_PASS=zabbix
export ZABBIX_URL=${ZABBIX_API_DEV:-"secondary.gorillass.co.uk:10052"}
export DOCKER_CERT_PATH=/tmp/certs
export DOCKER_HOST=tcp://us-east-1.docker.joyent.com:2376
export DOCKER_CLIENT_TIMEOUT=800
export COMPOSE_HTTP_TIMEOUT=800